package com.hhk.membermanagement.service.member;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.exception.member.CLoginMemberResignException;
import com.hhk.membermanagement.exception.member.CLoginPasswordFailedException;
import com.hhk.membermanagement.exception.common.CMissingDataException;
import com.hhk.membermanagement.model.member.MemberLoginRequest;
import com.hhk.membermanagement.model.member.MemberLoginResponse;
import com.hhk.membermanagement.model.attendance.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * 직원 로그인 서비스
 */
@Service
@RequiredArgsConstructor
public class MemberLoginService {
    private final MemberRepository memberRepository;

    /**
     * 로그인 서비스
     * @param isAdmin 관리자인지 확인
     * @param loginRequest 아이디 패스워드 정보
     * @return 직원 정보 (로그인 됐을때 토큰 사용을 위해 시퀀스값 담음)
     */
        public MemberLoginResponse getMemberLogin(boolean isAdmin, MemberLoginRequest loginRequest) {
        // 로그인시 아이디가 관리자인지 확인한 후 직원 정보 가져옴 없을시 예외처리 '데이터가 없습니다.'
        Member member = memberRepository.findByUsernameAndIsAdmin(loginRequest.getUsername(), isAdmin).orElseThrow(CMissingDataException::new);
        // 저장 돼어있는 패스워드와 입력받은 패스워드가 다르면 예외처리 '패스워드가 틀렸습니다.'
        if(!member.getPassword().equals(loginRequest.getPassword())) throw new CLoginPasswordFailedException();
        // 근로중이 아니면 예외처리 '퇴사직원 입니다. 관리자에게 문의하세요'
        if(!member.getIsWorking()) throw new CLoginMemberResignException();
        // 시퀀스값 리턴(토큰 사용을 위해)
        return new MemberLoginResponse.MemberLoginResponseBuilder(member).build();
    }
}
