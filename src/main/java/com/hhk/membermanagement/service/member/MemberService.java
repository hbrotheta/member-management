package com.hhk.membermanagement.service.member;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.exception.common.CMissingDataException;
import com.hhk.membermanagement.exception.member.CUserNameUsingException;
import com.hhk.membermanagement.model.common.ListResult;
import com.hhk.membermanagement.model.member.MemberAdminRequest;
import com.hhk.membermanagement.model.member.MemberDetailsResponse;
import com.hhk.membermanagement.model.member.MemberInfoDetailsRequest;
import com.hhk.membermanagement.model.member.MemberResignDateRequest;
import com.hhk.membermanagement.model.attendance.repository.MemberRepository;
import com.hhk.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * 직원 정보 서비스
 */
@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    /**
     * 해당 직원 정보 가져오는 메서드
     * @param memberId 직원 시퀀스
     * @return 직원 정보
     */
    public Member getMyData(long memberId) {
        // 해당 직원 정보 가져온 후 리턴 없으면 예외 처리 '데이터가 없습니다'
        return memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 직원 등록시 아이디 중복 확인 및 등록 메서드
     * @param request 직원 상세 정보
     * @return 직원 정보
     */
    public Member setMember(MemberInfoDetailsRequest request) {
        // 직원 정보에서 아이디 중복 확인
        Optional<Member> memberVerify = memberRepository.findAllByUsername(request.getUsername());

        // 중복 아이디가 있으면 예외처리 '아이디가 있습니다.'
        if (memberVerify.isPresent()) throw new CUserNameUsingException();

        // 없으면 직원 등록 후 직원정보 리턴
        Member member = new Member.MemberBuilder(request).build();
        return memberRepository.save(member);
    }

    /**
     * 해당 직원정보 가져오는 메서드 (관리자 용도)
     * @param memberId 직원 시퀀스
     * @return 직원 정보
     */
    public MemberDetailsResponse getMember(long memberId) {
        // 해당 직원의 정보를 가져오고 저장 후 리턴, 없으면 예외처리 '데이터가 없습니다.'
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        return new MemberDetailsResponse.MemberInfoDetailsResponseBuilder(member).build();
    }

    /**
     * 직원 리스트 가져오는 메서드(관리자 용도)
     * @return 직원정보
     */
    public ListResult<MemberDetailsResponse> getMembers() {
        // 직원 정보 전부 담아 리스트에 저장
        List<Member> members = memberRepository.findAll();

        // 재가공할 형태에 LinkedList 방식으로 연결
        List<MemberDetailsResponse> result = new LinkedList<>();

        // 가져왔던 리스트들 하나씩 꺼내서 재가공하고 저장
        members.forEach(member -> {
            MemberDetailsResponse addResponse = new MemberDetailsResponse.MemberInfoDetailsResponseBuilder(member).build();

            result.add(addResponse);
        });
        // 재가공한 직원 정보 리스트 리턴
        return ListConvertService.settingResult(result);
    }

    /**
     * 직원정보 수정 (관리자 용도)
     * @param memberId 해당직원
     * @param request 직원정보 수정값
     */
    public void putMember(long memberId, MemberInfoDetailsRequest request) {
        // 해당 직원 데이터를 가져온 후 저장
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        // 가져온 직원 데이터 안에 수정 메서드에 변경할값 전달
        member.putMember(request);
        // 수정된 직원 정보 저장
        memberRepository.save(member);
    }

    /**
     * 관리자 권한 부여 메서드 (관리자 용도)
     * @param memberId 직원 시퀀스
     * @param request 관리자 권한값
     */
    public void putMemberAdmin(long memberId, MemberAdminRequest request) {
        // 해당 직원 데이터를 가져온 후 저장
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        // 가져온 직원 데이터 안에 수정 메서드에 변경할값 전달
        member.putMemberAdmin(request);
        // 수정된 직원 정보 저장
        memberRepository.save(member);
    }

    /**
     * 퇴사 처리 메서드 (관리자 용도)
     * Delete 는 최대한 사용하지 않는다.
     * @param memberId 직원 시퀀스
     * @param request 퇴사처리 값
     */
    public void putMemberResign(long memberId, MemberResignDateRequest request) {
        // 해당 직원 데이터를 가져온 후 저장
        Member member = memberRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        // 근로중이 아니면 예외처리 '데이터를 찾을수 없습니다.'
        if (!member.getIsWorking()) throw new CMissingDataException();
        // 가져온 직원 데이터 안에 수정 메서드에 변경할값 전달
        member.putMemberResign(request);
        // 수정된 직원 정보 저장
        memberRepository.save(member);
    }
}
