package com.hhk.membermanagement.service.vacation;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.entity.VacationCount;
import com.hhk.membermanagement.entity.VacationTotalUsage;
import com.hhk.membermanagement.enums.vacation.ApprovalStatus;
import com.hhk.membermanagement.exception.common.CMissingDataException;
import com.hhk.membermanagement.exception.dailycheck.CDailyCheckStateSameException;
import com.hhk.membermanagement.exception.vacation.CVacationCountException;
import com.hhk.membermanagement.exception.vacation.CVacationCountRefusalException;
import com.hhk.membermanagement.model.common.ListResult;
import com.hhk.membermanagement.model.vacation.*;
import com.hhk.membermanagement.model.attendance.repository.VacationCountRepository;
import com.hhk.membermanagement.model.attendance.repository.VacationTotalUsageRepository;
import com.hhk.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

import static com.hhk.membermanagement.enums.vacation.ApprovalStatus.REFUSAL;

/**
 * 관리자용 - 휴가 관리 서비스
 */
@Service
@RequiredArgsConstructor
public class VacationService {
    private final VacationCountRepository vacationCountRepository;
    private final VacationTotalUsageRepository vacationTotalUsageRepository;

    /**
     * 직원 등록시 연차 기본세팅 (신입사원 기준으로 0개)
     * @param memberId 직원 시퀀스
     * @param dateJoin 입사일
     */
    public void setDefaultVacation(long memberId, LocalDate dateJoin) {
        // 직원 시퀀스와 입사일을 휴가 수량 테이블에 전달
        VacationCount vacationCount = new VacationCount.VacationBuilder(memberId, dateJoin).build();
        // 입사일 및 휴가 수량 저장
        vacationCountRepository.save(vacationCount);
    }

    /**
     * 나의 현재 연차 수량 정보 가져오기
     * @param memberId 직원 시퀀스
     * @return 연차 수량 정보
     */
    public VacationCountMyDataResponse getMyVacationCount(long memberId) {
        // 해당 직원의 휴가 수량 테이블 정보를 가져오고
        VacationCount myCount = vacationCountRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        // 가져온 연차 수량 정보를 재가공 해서 리턴
        return new VacationCountMyDataResponse.VacationCountMyDataResponseBuilder(myCount).build();
    }

    /**
     * 연차 기본 수량 부여 및 연차 수량 변경
     * @param memberId 직원 시퀀스
     * @param Request 연차 수량, 연차 기한
     */
    public void putVacationDefaultCount(long memberId, VacationDefaultCountRequest Request) {
        // 해당 직원의 휴가 수량 테이블 정보를 가져오고
        VacationCount vacationCount = vacationCountRepository.findById(memberId).orElseThrow(CMissingDataException::new);
        // 기본 휴가 수량 부여 및 연차 수량 변경
        vacationCount.putVacationDefaultCount(Request);
        // 변경한 데이터 저장
        vacationCountRepository.save(vacationCount);
    }

    /**
     * 직원이 휴가 신청시, 연차 수량 변경 및 휴가 신청내역에 등록
     * @param member 해당직원 테이블
     * @param request 휴가 신청 정보들
     */
    public void setMemberRequestVacation(Member member, VacationMemberRequest request) {
        VacationCount vacationCount = vacationCountRepository.findById(member.getId()).orElseThrow(CMissingDataException::new);

        // 총 연차 수량 보다 (사용한 연차 수량 + 신청한 연차 수량)이 크면 Exception 처리
        if (vacationCount.getVacationTotal() < (vacationCount.getVacationCount() + request.getIncreaseOrDecreaseValue())) throw new CVacationCountException();

        // 휴가 종합내역에 연차 신청 등록
        VacationTotalUsage vacationTotalUsage = new VacationTotalUsage.VacationApplyBuilder(member, request).build();
        vacationTotalUsageRepository.save(vacationTotalUsage);
    }

    /**
     * 나의 휴가 신청 리스트 가져오기 (필터x)
     * @param memberId 직원 시퀀스
     * @return 휴가 신청 리스트
     */
    public ListResult<VacationApplyItem> getMyVacationApplies(long memberId) {
        // 해당 직원의 전체 휴가 신청내역을 최근 순으로 가져온다
        List<VacationTotalUsage> vacationTotalUsages = vacationTotalUsageRepository.findAllByMember_IdOrderByIdDesc(memberId);
        // 재가공해서 전달할 데이터를 LinkedList 형식으로 연결한다.
        List<VacationApplyItem> result = new LinkedList<>();
        // 반복문을 통해 가져온 리스트들을 하나씩 꺼내어 재가공 후 저장한다.
        vacationTotalUsages.forEach(vacationTotalUsage -> {
            VacationApplyItem vacationApplyItem = new VacationApplyItem.MemberVacationApplyItemBuilder(vacationTotalUsage).build();
            result.add(vacationApplyItem);
        });
        // 재가공한 데이터를 데이터 수량 정보와 함께 리턴한다.
        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가 신청 리스트 전부가져오기
     * @return 휴가 신청 리스트
     */
    public ListResult<VacationApplyItem> getVacationApplies() {
        // 휴가 전체 신청내역을 최신순으로 가져온다
        List<VacationTotalUsage> vacationTotalUsages = vacationTotalUsageRepository.findAllByOrderByIdDesc();
        // 재가공해서 전달할 데이터를 LinkedList 형식으로 연결한다.
        List<VacationApplyItem> result = new LinkedList<>();
        // 반복문을 통해 가져온 리스트들을 하나씩 꺼내어 재가공 후 저장한다.
        vacationTotalUsages.forEach(vacationTotalUsage -> {
            VacationApplyItem vacationApplyItem = new VacationApplyItem.MemberVacationApplyItemBuilder(vacationTotalUsage).build();
            result.add(vacationApplyItem);
        });
        // 재가공한 데이터를 데이터 수량 정보와 함께 리턴한다.
        return ListConvertService.settingResult(result);
    }

    /**
     *  전체 휴가 신청 리스트 필터로 가져오기 (대기중, 승인, 반려)
     * @param approvalStatus 대기,승인,반려 enum
     * @return 휴가 신청 리스트
     */
    public ListResult<VacationApplyItem> getVacationApplyListFilter(ApprovalStatus approvalStatus) {
        // 승인 or 대기중 or 반려에 해당하는 휴가 내역 리스트를 가져온다.
        List<VacationTotalUsage> vacationTotalUsages = vacationTotalUsageRepository.findAllByApprovalStatusOrderByDateApprovingDesc(approvalStatus);
        // 재가공해서 전달할 데이터를 LinkedList 형식으로 연결한다.
        List<VacationApplyItem> result = new LinkedList<>();
        // 반복문을 통해 가져온 리스트들을 하나씩 꺼내어 재가공 후 저장한다.
        vacationTotalUsages.forEach(vacationTotalUsage -> {
            VacationApplyItem vacationApplyItem = new VacationApplyItem.MemberVacationApplyItemBuilder(vacationTotalUsage).build();
            result.add(vacationApplyItem);
        });
        // 재가공한 데이터를 데이터 수량 정보와 함께 리턴한다.
        return ListConvertService.settingResult(result);
    }

    /**
     * 휴가 신청 내역을 가져온다 (상세내역 페이지 용도)
     * @param vacationTotalUsageId 휴가신청내역 시퀀스
     * @return 휴가 신청내역(단수)
     */
    public VacationTotalUsage getVacationApprovingData(long vacationTotalUsageId) {
        // 해당 휴가 신청내역 가져온다 없으면 예외처리 '데이터가 없습니다.'
        return vacationTotalUsageRepository.findById(vacationTotalUsageId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 휴가 신청 내역을 갖고와 승인 or 반려 처리하고 연차 수량 변경하기
     * @param vacationTotalUsage 휴가 신청 내역(단수)
     * @param approvalStatus 승인, 대기, 반려
     */
    public void putVacationApproval(VacationTotalUsage vacationTotalUsage, ApprovalStatus approvalStatus) {
        // 애초에 대기 상태의 정보만 갖고 왔지만 2중 보안처리, 동일한 승인 상태로 변경할수 없습니다 처리
        if (approvalStatus.equals(vacationTotalUsage.getApprovalStatus())) throw new CDailyCheckStateSameException();

        VacationCount vacationCount = vacationCountRepository.findById(vacationTotalUsage.getMember().getId()).orElseThrow(CMissingDataException::new);

        // 총 연차 수량 확인에 관한 if문
        // 총 연차 수량 보다 (사용한 연차 수량 + 신청한 연차 수량))이 크면 (총 연차 수량을 넘어간다는뜻)
        // 반려상태로 저장 그게 아니면 승인 상태로 저장
        if (vacationCount.getVacationTotal() < (vacationCount.getVacationCount() + vacationTotalUsage.getVacationApplyValue())) {
            vacationTotalUsage.putVacationApproval(vacationTotalUsage.getVacationApplyValue(), REFUSAL);
            vacationTotalUsageRepository.save(vacationTotalUsage);
            throw new CVacationCountRefusalException();
        } else { // 정상적인 신청이였다면 승인상태와 연차 수량 변경
            vacationTotalUsage.putVacationApproval(vacationTotalUsage.getVacationApplyValue(), approvalStatus);
            vacationTotalUsageRepository.save(vacationTotalUsage);
            // 휴가가 승인이면 연차 수량 테이블에 실제 반영값 증가 시키기
            vacationCount.plusVacationCount(vacationTotalUsage.getFixIncreaseOrDecreaseValue());
            vacationCountRepository.save(vacationCount);
        }
    }

    /**
     * 휴가 신청 리스트 가져오기 승인상태 + 기간 필터 + 부서 필터 (테스트x)
     * @param pageNum 해당 페이지 숫자
     * @param approvalStatus 승인상태
     * @param searchRequest 기간 + 부서 + 해당직원 담겨있음
     * @return 전달할 재가공 데이터
     */
    public ListResult<VacationApplyItem> getListByAdmin(int pageNum, ApprovalStatus approvalStatus, VacationSearchRequest searchRequest) {
        Page<VacationTotalUsage> originList;
        PageRequest pageRequest = PageRequest.of(pageNum, 10);

        LocalDateTime dateStart = LocalDateTime.of(
                searchRequest.getDateStart().getYear(),
                searchRequest.getDateStart().getMonth(),
                searchRequest.getDateStart().getDayOfMonth(),
                0,
                0,
                0);

        LocalDateTime dateEnd = LocalDateTime.of(
                searchRequest.getDateEnd().getYear(),
                searchRequest.getDateEnd().getMonth(),
                searchRequest.getDateEnd().getDayOfMonth(),
                23,
                59,
                59);

        if (searchRequest.getTeam() == null) {
            originList = vacationTotalUsageRepository.findAllByApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(approvalStatus, dateStart, dateEnd, pageRequest);
        } else originList = vacationTotalUsageRepository.findAllByMember_TeamAndApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(searchRequest.getTeam(), approvalStatus, dateStart, dateEnd, pageRequest);

        List<VacationApplyItem> result = new LinkedList<>();

        originList.getContent().forEach(origin -> {
            VacationApplyItem addItem = new VacationApplyItem.MemberVacationApplyItemBuilder(origin).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    }

    /**
     * 연차 승인까지 받았는데 취소해야 할때 (시간나면 만들자)
     * @param member
     * @param adminRequest
     */

//    public void setVacation(Member member, AdminVacationStateDataRequest adminRequest) {
//        VacationCount vacationCount = vacationCountRepository.findById(member.getId()).orElseThrow(CMissingDataException::new);
//
//        if (adminRequest.getIsMinus()) vacationCount.plusCountUsing(adminRequest.getFixIncreaseOrDecreaseValue());
//        else vacationCount.plusCountTotal(adminRequest.getFixIncreaseOrDecreaseValue());
//
//        VacationTotalUsage vacationTotalUsage =  new VacationTotalUsage.AdminVacationApplyBuilder(member, adminRequest).build();
//        vacationTotalUsageRepository.save(vacationTotalUsage);
//    }
}
