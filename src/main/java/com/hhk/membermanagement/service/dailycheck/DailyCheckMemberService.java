package com.hhk.membermanagement.service.dailycheck;

import com.hhk.membermanagement.entity.DailyCheck;
import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.enums.dailycheck.DailyCheckState;
import com.hhk.membermanagement.exception.dailycheck.CDailyCheckChangeWorkEndException;
import com.hhk.membermanagement.exception.dailycheck.CDailyCheckNotChangeWorkEndException;
import com.hhk.membermanagement.exception.dailycheck.CDailyCheckStateSameException;
import com.hhk.membermanagement.model.common.ListResult;
import com.hhk.membermanagement.model.dailycheck.DailyCheckMyDataResponse;
import com.hhk.membermanagement.model.dailycheck.DailyCheckResponse;
import com.hhk.membermanagement.model.attendance.repository.DailyCheckRepository;
import com.hhk.membermanagement.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

/**
 * 출퇴근 관리 서비스
 */
@Service
@RequiredArgsConstructor
public class DailyCheckMemberService {
    private final DailyCheckRepository dailyCheckRepository;

    /**
     * 현재 어떤 상태인지 확인 메서드
     * @param member 직원 시퀀스
     * @return 현재 상태
     */
    public DailyCheckResponse getCurrentState(Member member) {
        // 해당 직원이 현재 시간에 기록이 있는지 확인
        Optional<DailyCheck> dailyCheck = dailyCheckRepository.findByDateBaseAndMember_Id(LocalDate.now(), member.getId());

        // 해당 직원의 데이터가 없으면 알수없음 상태 리턴
        if (dailyCheck.isEmpty()) return new DailyCheckResponse.DailyCheckResponseUnknownBuilder().build();
        // 데이터가 있으면 현재 상태 리턴
        else return new DailyCheckResponse.DailyCheckResponseBuilder(dailyCheck.get()).build();
    }

    /**
     * 출퇴근 상태 확인 후 상태 변경 메서드
     * @param member 해당 직원
     * @param dailyCheckState 출/외/복/퇴 변경상태값 enum
     * @return 현재 상태
     */
    public DailyCheckResponse doDailyCheckChange(Member member, DailyCheckState dailyCheckState) {
        // 해당 직원이 현재 시간에 기록이 있는지 확인
        Optional<DailyCheck> dailyCheck = dailyCheckRepository.findByDateBaseAndMember_Id(LocalDate.now(), member.getId());

        // 기록이 없을시 출퇴근 정보에 해당 직원 저장(출근상태 처리됨)
        DailyCheck dailyCheckResult;
        if (dailyCheck.isEmpty()) dailyCheckResult = setDailyCheck(member);

        // 기록이 있으면 출근이므로 상태 수정 메서드로 이동
        else dailyCheckResult = putDailyCheck(dailyCheck.get(), dailyCheckState);

        return new DailyCheckResponse.DailyCheckResponseBuilder(dailyCheckResult).build();
    }

    /**
     * 출근 처리 등록 메서드
     * @param member 직원정보
     * @return 출퇴근 정보
     */
    private DailyCheck setDailyCheck(Member member) {
        // 출퇴근 테이블에 해당 직원 출근 처리
        DailyCheck data = new DailyCheck.DailyCheckBuilder(member).build();
        return dailyCheckRepository.save(data);
    }

    /**
     * 출근 상태에서 외출/복귀/퇴근 상태로 변경 메서드
     * @param dailyCheck 출퇴근 상태 테이블
     * @param dailyCheckState 출/외/복/퇴 상태 enum
     * @return 출퇴근 정보
     */
    private DailyCheck putDailyCheck(DailyCheck dailyCheck, DailyCheckState dailyCheckState) {
        // 출근 후에는 다시 출근상태로 변경 할 수 없다.
        if (dailyCheckState.equals(DailyCheckState.WORK_START)) throw  new CDailyCheckChangeWorkEndException();

        // 같은 상태로는 변경 할 수 없다.
        if (dailyCheck.getDailyCheckState().equals(dailyCheckState)) throw  new CDailyCheckStateSameException();

        // 퇴근 후에는 상태를 변경 할 수 없다.
        if (dailyCheck.getDailyCheckState().equals(DailyCheckState.WORK_END)) throw new CDailyCheckNotChangeWorkEndException();

        dailyCheck.putDailyCheckState(dailyCheckState);
        return dailyCheckRepository.save(dailyCheck);
    }

    /**
     * 해당 직원의 출퇴근 리스트 가져오는 메서드
     * @param memberId 직원 시퀀스
     * @return 출퇴근 정보 리스트
     */
    public ListResult<DailyCheckMyDataResponse> getDailyCheckMyData(long memberId) {
        List<DailyCheck> dailyChecks = dailyCheckRepository.findAllByMember_IdOrderByDateBaseDesc(memberId);

        List<DailyCheckMyDataResponse> result = new LinkedList<>();

        dailyChecks.forEach(dailyCheck -> {
            DailyCheckMyDataResponse dataResponse = new DailyCheckMyDataResponse.DailyCheckMyDataResponseBuilder(dailyCheck).build();
            result.add(dataResponse);
        });
        return ListConvertService.settingResult(result);
    }

    //todo 달 검색하여 해당 달의 근무일수 구해보자
//    public MyDailyCheckItem getMyDailyCheck(long memberId, int Year, int month) {
//        DailyCheck dailyCheck = dailyCheckRepository.findById(memberId).orElseThrow(CMissingDataException::new);
//
//        return new MyDailyCheckItem.MyDailyCheckItemBuilder(dailyCheck, searchDate).build();
//    }
}

