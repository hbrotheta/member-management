package com.hhk.membermanagement.service.todo;

import com.hhk.membermanagement.entity.todo.HolidayCount;
import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.model.todo.MyHolidayCountResponse;
import com.hhk.membermanagement.model.attendance.repository.todo.HolidayCountRepository;
import com.hhk.membermanagement.model.attendance.repository.todo.HolidayRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Optional;

//todo 몇년 차인지 계산 서비스
@Service
@RequiredArgsConstructor
public class HolidayService {
    // 연차 신청내역과 연차 잔여수는 떨어질 수 없는 관계이기 때문에 holidayService 안에서 같이 묶음.
    private final HolidayRepository holidayRepository;
    private final HolidayCountRepository holidayCountRepository;

    // 연차 초기값 등록
    public void setDefaultCount(Member member, LocalDate dateCriteria) {
        // 기준일을 가지고 임의의 연차시작일을 만든다.
        LocalDate dateStart = getCriteriaDateStart(member, dateCriteria);

        // 연차시작일 <= 조회 기준일 and 연차종료일 >= 조회기준일 and 해당회원 조건에 해당하는 데이터를 가져온다.
        Optional<HolidayCount> holidayCountCheck = holidayCountRepository.findByDateStartLessThanEqualAndDateEndGreaterThanEqualAndMember(dateCriteria, dateCriteria, member);

        if (holidayCountCheck.isEmpty()) {
            // 빌더를 통해 초기값 세팅
            HolidayCount holidayCount = new HolidayCount.HolidayCountBuilder(member, dateStart).build();

            // DB 저장
            holidayCountRepository.save(holidayCount);
        }
    }

    // 연차 개수 정보
    public MyHolidayCountResponse getMyCount(Member member, LocalDate dateCriteria) {
        // 연차시작일 <= 조회 기준일 and 연차종료일 >= 조회기준일 and 해당회원 조건에 해당하는 데이터를 가져온다.
        Optional<HolidayCount> holidayCount = holidayCountRepository.findByDateStartLessThanEqualAndDateEndGreaterThanEqualAndMember(dateCriteria, dateCriteria, member);

        // 만약에 데이터가 있으면 (등록된 연차 초기화 데이터가 있으면)
        if (holidayCount.isPresent()) {
            // 데이터에서 기준 시작일 가져오고
            LocalDate dateStart = holidayCount.get().getDateStart();

            // 데이터에서 기준 종료일 가져오고
            LocalDate dateEnd = holidayCount.get().getDateEnd();

            // 위의 기간에 연차 신청/승인 된 수 가져오고
            long countComplete = holidayRepository.countByMemberAndDateHolidayRequestGreaterThanEqualAndDateHolidayRequestLessThanEqualAndIsComplete(member, dateStart, dateEnd, true);

            // 빌더로 값을 넣고 리턴. (기본빌더)
            return new MyHolidayCountResponse.MyHolidayCountResponseBuilder(holidayCount.get(), countComplete).build();
        } else { // 등록된 연차 초기화 데이터가 없으면
            // 임의로 연차 시작일을 구하고
            LocalDate dateStart = getCriteriaDateStart(member, dateCriteria);
            // 연차 시작일에서 1년을 더하고 1일을 빼고
            LocalDate dateEnd = dateStart.plusYears(1).minusDays(1);

            // 임의로 구한 기간에 연차 신청/승인된 수를 가져오고
            long countComplete = holidayRepository.countByMemberAndDateHolidayRequestGreaterThanEqualAndDateHolidayRequestLessThanEqualAndIsComplete(member, dateStart, dateEnd, true);

            // 빌더로 값을 넣고 리턴. 위의 빌더와 다른 것 주의.
            return new MyHolidayCountResponse.MyHolidayCountResponseEmptyBuilder(dateStart, dateEnd, countComplete).build();
        }
    }

    // 등록된 초기연차가 없을 때 기준일을 기준으로 연차 시작일을 구하는 메서드
    private LocalDate getCriteriaDateStart(Member member, LocalDate dateCriteria) {
        // 기준일의 년도를 가지고 몇년차인지 구한다.
        // +1의 이유는 예를들면 2020년부터 2022년 근무라고 했을때 2020, 2021, 2022 총 3개를 세야하니까.
        int perYear = dateCriteria.getYear() - member.getDateJoin().getYear() + 1;

        // 입사일 기준으로 년차에 해당하는 연차 기준 시작일을 만들어 리턴한다.
        return LocalDate.of(member.getDateJoin().getYear() + (perYear - 1), member.getDateJoin().getMonthValue(), member.getDateJoin().getDayOfMonth());
    }
}
