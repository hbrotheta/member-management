package com.hhk.membermanagement.model.common;

import lombok.Getter;
import lombok.Setter;

/**
 * 성공 실패 결과 용도
 */
@Getter
@Setter
public class CommonResult {
    private Boolean isSuccess;
    private Integer code;
    private String msg;
}
