package com.hhk.membermanagement.model.dailycheck;

import com.hhk.membermanagement.entity.DailyCheck;
import com.hhk.membermanagement.enums.dailycheck.DailyCheckState;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 출퇴근 처리 한글 값 및 enum 값 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheckResponse {
    @ApiModelProperty(value = "한글(출근/외근/복귀/퇴근) 상태")
    private String dailyCheckStateName;
    @ApiModelProperty(value = "enum 키값(예 'WORK_START')")
    private String dailyCheckState;

    private DailyCheckResponse(DailyCheckResponseBuilder builder) {
        this.dailyCheckStateName = builder.dailyCheckStateName;
        this.dailyCheckState = builder.dailyCheckState;
    }
    private DailyCheckResponse(DailyCheckResponseUnknownBuilder builder) {
        this.dailyCheckStateName = builder.dailyCheckStateName;
        this.dailyCheckState = builder.dailyCheckState;
    }
    public static class DailyCheckResponseBuilder implements CommonModelBuilder<DailyCheckResponse> {
        private final String dailyCheckStateName;
        private final String dailyCheckState;

        public DailyCheckResponseBuilder(DailyCheck dailyCheck) {
            this.dailyCheckStateName = dailyCheck.getDailyCheckState().getName();
            this.dailyCheckState = dailyCheck.getDailyCheckState().toString();
        }
        @Override
        public DailyCheckResponse build() {
            return new DailyCheckResponse(this);
        }
    }

    public static class DailyCheckResponseUnknownBuilder implements CommonModelBuilder<DailyCheckResponse> {
        private final String dailyCheckStateName;
        private final String dailyCheckState;

        public DailyCheckResponseUnknownBuilder() {
            this.dailyCheckStateName = DailyCheckState.UNKNOWN.getName();
            this.dailyCheckState = DailyCheckState.UNKNOWN.toString();
        }
        @Override
        public DailyCheckResponse build() {
            return new DailyCheckResponse(this);
        }
    }
}

