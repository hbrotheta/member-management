package com.hhk.membermanagement.model.dailycheck;

import com.hhk.membermanagement.entity.DailyCheck;
import com.hhk.membermanagement.enums.dailycheck.DailyCheckState;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * 직원 출퇴근 상태 정보 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheckStateResponse {
    @ApiModelProperty(value = "출근/외근/복귀/퇴근 상태")
    @Enumerated(value = EnumType.STRING)
    private DailyCheckState dailyCheckState;
    private DailyCheckStateResponse(DailyCheckStateResponseBuilder builder) {
        this.dailyCheckState = builder.dailyCheckState;
    }
    public static class DailyCheckStateResponseBuilder implements CommonModelBuilder<DailyCheckStateResponse> {
        private final DailyCheckState dailyCheckState;
        public DailyCheckStateResponseBuilder(DailyCheck dailyCheck) {
            this.dailyCheckState = dailyCheck.getDailyCheckState();
        }
        @Override
        public DailyCheckStateResponse build() {
            return new DailyCheckStateResponse(this);
        }
    }
}
