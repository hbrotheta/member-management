package com.hhk.membermanagement.model.dailycheck;

import com.hhk.membermanagement.entity.DailyCheck;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * 나의 출근 정보 용도(출/외/지각 카운트 미구현)
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheckMyDataResponse {
    @ApiModelProperty(value = "직원 이름")
    private String memberName;
    @ApiModelProperty(value = "출근일")
    private Integer workStartCount;
    @ApiModelProperty(value = "외출일")
    private Integer shortOutingCount;
    @ApiModelProperty(value = "지각일")
    private Integer lateNessCount;
    @ApiModelProperty(value = "기준일")
    private LocalDate dateBase;
    @ApiModelProperty(value = "출근 시간")
    private LocalTime dateWorkStart;
    @ApiModelProperty(value = "외출 시간")
    private LocalTime dateShortOuting;
    @ApiModelProperty(value = "복귀 시간")
    private LocalTime dateWorkComeBack;
    @ApiModelProperty(value = "퇴근 시간")
    private LocalTime dateWorkEnd;
    private DailyCheckMyDataResponse(DailyCheckMyDataResponseBuilder builder) {
        this.memberName = builder.memberName;
//        this.workStartCount = builder.workStartCount;
//        this.shortOutingCount = builder.shortOutingCount;
//        this.lateNessCount = builder.lateNessCount;
        this.dateBase = builder.dateBase;
        this.dateWorkStart = builder.dateWorkStart;
        this.dateShortOuting = builder.dateShortOuting;
        this.dateWorkComeBack = builder.dateWorkComeBack;
        this.dateWorkEnd = builder.dateWorkEnd;
    }

    public static class DailyCheckMyDataResponseBuilder implements CommonModelBuilder<DailyCheckMyDataResponse> {
        private final String memberName;
//        private final Integer workStartCount;
//        private final Integer shortOutingCount;
//        private final Integer lateNessCount;
        private final LocalDate dateBase;
        private final LocalTime dateWorkStart;
        private final LocalTime dateShortOuting;
        private final LocalTime dateWorkComeBack;
        private final LocalTime dateWorkEnd;

        public DailyCheckMyDataResponseBuilder(DailyCheck dailyCheck) {
            this.memberName = dailyCheck.getMember().getMemberName();
//            this.workStartCount = workStartCount;
//            this.shortOutingCount = shortOutingCount;
//            this.lateNessCount = lateNessCount;
            this.dateBase = dailyCheck.getDateBase();
            this.dateWorkStart = dailyCheck.getDateWorkStart();
            this.dateShortOuting = dailyCheck.getDateShortOuting();
            this.dateWorkComeBack = dailyCheck.getDateWorkComeBack();
            this.dateWorkEnd = dailyCheck.getDateWorkEnd();
        }

        @Override
        public DailyCheckMyDataResponse build() {
            return new DailyCheckMyDataResponse(this);
        }
    }
}
