package com.hhk.membermanagement.model.dailycheck;

import com.hhk.membermanagement.enums.dailycheck.DailyCheckState;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * (관리자) 출퇴근 정보 수정용
 */
@Getter
@Setter
public class DailyCheckStateRequest {
    @ApiModelProperty(value = "출근/외근/복귀/퇴근 상태")
    @Enumerated(value = EnumType.STRING)
    @NotNull
    private DailyCheckState dailyCheckState;
}
