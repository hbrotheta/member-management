package com.hhk.membermanagement.model.attendance.repository;

import com.hhk.membermanagement.entity.VacationTotalUsage;
import com.hhk.membermanagement.enums.member.Team;
import com.hhk.membermanagement.enums.vacation.ApprovalStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface VacationTotalUsageRepository extends JpaRepository<VacationTotalUsage, Long> {
    // 해당 직원 휴가 신청 내역 최신순으로 가져오기
    List<VacationTotalUsage> findAllByMember_IdOrderByIdDesc(Long memberId);

    // 전체 휴가 신청 내역 최신순으로 가져오기
    List<VacationTotalUsage> findAllByOrderByIdDesc();

    // 승인상태(승인,대기중,반려)로 휴가신청내역 최신순으로 가져오기
    List<VacationTotalUsage> findAllByApprovalStatusOrderByDateApprovingDesc(ApprovalStatus approvalStatus);

    // 승인상태 + 기간필터 page 형태로 상태 가져오기 (필터용도)
    Page<VacationTotalUsage> findAllByApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(ApprovalStatus approvalStatus, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);

    // 팀별 + 승인상태 + 기간필터 page 형태로 상태 가져오기 (필터용도)
    Page<VacationTotalUsage> findAllByMember_TeamAndApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(Team team, ApprovalStatus approvalStatus, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);

    // 해당 직원 승인상태 + 기간필터 page 형태로 상태 가져오기 (필터용도)
    Page<VacationTotalUsage> findAllByMember_IdAndApprovalStatusAndDateApprovingGreaterThanEqualAndDateApprovingLessThanEqualOrderByDateApprovingDesc(Long member_Id, ApprovalStatus approvalStatus, LocalDateTime dateStart, LocalDateTime dateEnd, Pageable pageable);
}
