package com.hhk.membermanagement.model.attendance.repository;

import com.hhk.membermanagement.entity.DailyCheck;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface DailyCheckRepository extends JpaRepository<DailyCheck, Long> {
    // 해당 직원의 출퇴근 리스트 최신순으로 가져오기
    List<DailyCheck> findAllByMember_IdOrderByDateBaseDesc(Long memberId);
    // 해당 직원의 기준일로 출퇴근 리스트 가져오기
    Optional<DailyCheck> findByDateBaseAndMember_Id(LocalDate dateBase, Long memberId);

    //todo 해당직원의 출근일수 가져오기
    long countByMember_IdAndDateBaseGreaterThanEqualAndDateBaseLessThanEqual(Long memberId, LocalDate dateStart, LocalDate dateEnd);
}
