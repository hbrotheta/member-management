package com.hhk.membermanagement.model.attendance.repository;

import com.hhk.membermanagement.entity.VacationCount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacationCountRepository extends JpaRepository<VacationCount, Long> {
}
