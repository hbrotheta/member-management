package com.hhk.membermanagement.model.attendance.repository.todo;

import com.hhk.membermanagement.entity.todo.Holiday;
import com.hhk.membermanagement.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;

public interface HolidayRepository extends JpaRepository<Holiday, Long> {
    long countByMemberAndDateHolidayRequestGreaterThanEqualAndDateHolidayRequestLessThanEqualAndIsComplete(Member member, LocalDate dateStart, LocalDate dateEnd, boolean isComplete);
}
