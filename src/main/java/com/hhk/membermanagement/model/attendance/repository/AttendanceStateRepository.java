package com.hhk.membermanagement.model.attendance.repository;

import com.hhk.membermanagement.entity.AttendanceState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttendanceStateRepository extends JpaRepository<AttendanceState, Long> {
}
