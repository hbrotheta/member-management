package com.hhk.membermanagement.model.attendance.repository;

import com.hhk.membermanagement.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    // 아이디를 받아 관리자 or 직원인지 확인
    Optional<Member> findByUsernameAndIsAdmin(String username, boolean isAdmin);
    // 기존 직원 아이디가 있는지 없는지 확인 (unique = true 상태로 중복 불가)
    Optional<Member> findAllByUsername(String  userName);
}
