package com.hhk.membermanagement.model.attendance.repository.todo;

import com.hhk.membermanagement.entity.todo.HolidayCount;
import com.hhk.membermanagement.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface HolidayCountRepository extends JpaRepository<HolidayCount, Long> {
    Optional<HolidayCount> findByDateStartLessThanEqualAndDateEndGreaterThanEqualAndMember(LocalDate dateCriteria1, LocalDate dateCriteria2, Member member);
}
