package com.hhk.membermanagement.model.attendance;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.enums.vacation.ApprovalStatus;
import com.hhk.membermanagement.enums.attendance.ShiftWorkType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * 근태 수정용
 */
@Getter
@Setter
public class AttendanceStateInfoRequest {

    @NotNull
    @ApiModelProperty(name = "member 시퀀스")
    private Member memberId;

    @NotNull
    @Length(max = 40)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(name = "근태 종류")
    private ShiftWorkType shiftWorkType;

    @NotNull
    @Length(max = 30)
    @ApiModelProperty(name = "근태 변경 사유")
    private String shiftWorkReason;

    @NotNull
    @Length(max = 30)
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(name = "승인 상태")
    private ApprovalStatus approvalStatus;

    @NotNull
    @ApiModelProperty(name = "근무 시간")
    private Short workTime;
}
