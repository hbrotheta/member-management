package com.hhk.membermanagement.model.vacation;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * 휴가 기본 세팅을 위한 용도
 */
@Getter
@Setter
public class VacationDefaultCountRequest {

    @ApiModelProperty(value = "총 연차 수량")
    @NotNull
    private Float vacationTotal;

    @ApiModelProperty(value = "연차 유효 시작일")
    private LocalDate dateVacationStart;
}
