package com.hhk.membermanagement.model.vacation;

import com.hhk.membermanagement.entity.VacationTotalUsage;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 휴가신청 상세 페이지 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationApplyDetailsItem {
    @ApiModelProperty(value = "종합 휴가 내역 시퀀스")
    private Long vacationTotalUsageId;
    @ApiModelProperty(value = "승인 상태")
    private String approvalStatus;
    @ApiModelProperty(value = "부서명")
    private String team;
    @ApiModelProperty(value = "이름")
    private String memberName;
    @ApiModelProperty(value = "연락처")
    private String memberPhone;
    @ApiModelProperty(value = "직급")
    private String position;
    @ApiModelProperty(value = "연차 종류")
    private String vacationType;
    @ApiModelProperty(value = "신청일자")
    private LocalDateTime dateApproving;
    @ApiModelProperty(value = "신청 사유")
    private String vacationReason;
    @ApiModelProperty(value = "연차 신청 수량")
    private String vacationApplyValue;
    @ApiModelProperty(value = "휴가 시작일")
    private LocalDate vacationStart;
    @ApiModelProperty(value = "휴가 종료일")
    private LocalDate vacationEnd;

    private VacationApplyDetailsItem(VacationDetailsItemBuilder builder) {
        this.vacationTotalUsageId = builder.vacationTotalUsageId;
        this.approvalStatus = builder.approvalStatus;
        this.team = builder.team;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.position = builder.position;
        this.vacationType = builder.vacationType;
        this.dateApproving = builder.dateApproving;
        this.vacationReason = builder.vacationReason;
        this.vacationApplyValue = builder.vacationApplyValue;
        this.vacationStart = builder.vacationStart;
        this.vacationEnd = builder.vacationEnd;
    }

    public static class VacationDetailsItemBuilder implements CommonModelBuilder<VacationApplyDetailsItem> {
        private final Long vacationTotalUsageId;
        private final String approvalStatus;
        private final String team;
        private final String memberName;
        private final String memberPhone;
        private final String position;
        private final String vacationType;
        private final LocalDateTime dateApproving;
        private final String vacationReason;
        private final String vacationApplyValue;
        private final LocalDate vacationStart;
        private final LocalDate vacationEnd;

        public VacationDetailsItemBuilder(VacationTotalUsage vacationTotalUsage) {
            this.vacationTotalUsageId = vacationTotalUsage.getId();
            this.approvalStatus = vacationTotalUsage.getApprovalStatus().getName();
            this.team = vacationTotalUsage.getMember().getTeam().getName();
            this.memberName = vacationTotalUsage.getMember().getMemberName();
            this.memberPhone = vacationTotalUsage.getMember().getMemberPhone();
            this.position = vacationTotalUsage.getMember().getPosition().getName();
            this.vacationType = vacationTotalUsage.getVacationType().getName();
            this.dateApproving = vacationTotalUsage.getDateApproving();
            this.vacationReason = vacationTotalUsage.getVacationReason();
            this.vacationApplyValue = vacationTotalUsage.getVacationApplyValue().toString();
            this.vacationStart = vacationTotalUsage.getVacationStart();
            this.vacationEnd = vacationTotalUsage.getVacationEnd();
        }

        @Override
        public VacationApplyDetailsItem build() {
            return new VacationApplyDetailsItem(this);
        }
    }
}
