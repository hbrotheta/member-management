package com.hhk.membermanagement.model.vacation;

import com.hhk.membermanagement.entity.VacationTotalUsage;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.format.TextStyle;
import java.util.Locale;

/**
 * 휴가 신청 리스트 용도
 * */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationApplyItem {
    @ApiModelProperty(value = "종합 휴가 내역 시퀀스")
    private Long vacationTotalUsageId;
    @ApiModelProperty(value = "승인 상태")
    private String approvalStatus;
    @ApiModelProperty(value = "부서명")
    private String team;
    @ApiModelProperty(value = "이름")
    private String memberName;
    @ApiModelProperty(value = "직급")
    private String position;
    @ApiModelProperty(value = "연차 종류")
    private String vacationType;
    @ApiModelProperty(value = "신청일자")
    private String dateApproving;
    @ApiModelProperty(value = "연차 신청 수량")
    private String vacationApplyValue;

    private VacationApplyItem(MemberVacationApplyItemBuilder builder) {
        this.vacationTotalUsageId = builder.vacationTotalUsageId;
        this.approvalStatus = builder.approvalStatus;
        this.team = builder.team;
        this.memberName = builder.memberName;
        this.position = builder.position;
        this.vacationType = builder.vacationType;
        this.dateApproving = builder.dateApproving;
        this.vacationApplyValue = builder.vacationApplyValue;
    }
    public static class MemberVacationApplyItemBuilder implements CommonModelBuilder<VacationApplyItem> {
        private final Long vacationTotalUsageId;
        private final String approvalStatus;
        private final String team;
        private final String memberName;
        private final String position;
        private final String vacationType;
        private final String dateApproving;
        private final String vacationApplyValue;
        public MemberVacationApplyItemBuilder(VacationTotalUsage vacationTotalUsage) {
            this.vacationTotalUsageId = vacationTotalUsage.getId();
            this.approvalStatus = vacationTotalUsage.getApprovalStatus().getName();
            this.team = vacationTotalUsage.getMember().getTeam().getName();
            this.memberName = vacationTotalUsage.getMember().getMemberName();
            this.position = vacationTotalUsage.getMember().getPosition().getName();
            this.vacationType = vacationTotalUsage.getVacationType().getName();
            this.dateApproving = vacationTotalUsage.getDateApproving().toLocalDate().toString() + ' ' + '(' + vacationTotalUsage.getDateApproving().getDayOfWeek().getDisplayName(TextStyle.SHORT, Locale.KOREAN) + ')';
            this.vacationApplyValue = vacationTotalUsage.getVacationApplyValue().toString();
        }
        @Override
        public VacationApplyItem build() {
            return new VacationApplyItem(this);
        }
    }
}
