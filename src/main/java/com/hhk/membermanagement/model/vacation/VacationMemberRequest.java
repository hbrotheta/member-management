package com.hhk.membermanagement.model.vacation;

import com.hhk.membermanagement.enums.vacation.VacationType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * 연차 신청 용도
 */
@Getter
@Setter
public class VacationMemberRequest {
    @ApiModelProperty(value = "휴가 종류")
    @NotNull
    @Enumerated(EnumType.STRING)
    private VacationType vacationType;

    @ApiModelProperty(value = "휴가 시작일")
    @NotNull
    private LocalDate vacationStart;

    @ApiModelProperty(value = "휴가 종료일")
    @NotNull
    private LocalDate vacationEnd;

    @ApiModelProperty(value = "신청 사유")
    @NotNull
    @Length(max = 20)
    private String vacationReason;

    @ApiModelProperty(value = "증감 개수")
    @NotNull
    private Float increaseOrDecreaseValue;
}



