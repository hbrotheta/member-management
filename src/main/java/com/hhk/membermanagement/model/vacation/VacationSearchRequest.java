package com.hhk.membermanagement.model.vacation;

import com.hhk.membermanagement.enums.member.Team;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * 부서별 기간별 조회 용도
 */
@Getter
@Setter
public class VacationSearchRequest {
    @ApiModelProperty(value = "직원 시퀀스")
    private Long memberId;

    @ApiModelProperty(value = "부서 별 조회")
    private Team team;

    @ApiModelProperty(value = "휴가 신청기간 조회(시작일)", required = true)
    @NotNull
    private LocalDate dateStart;

    @ApiModelProperty(value = "휴가 신청기간 조회(종료일)", required = true)
    @NotNull
    private LocalDate dateEnd;
}
