package com.hhk.membermanagement.model.vacation;

import com.hhk.membermanagement.entity.VacationCount;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * 나의 연차 수량 정보 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationCountMyDataResponse {
    @ApiModelProperty(value = "총 휴가 수량")
    private String vacationTotal;
    @ApiModelProperty(value = "사용한 연차 수량")
    private String vacationCount;
    @ApiModelProperty(value = "남아있는 연차 수량")
    private String remainCount;
    @ApiModelProperty(value = "연차 유효 기간 시작일")
    private LocalDate dateVacationStart;
    @ApiModelProperty(value = "연차 유효 기간 종료일")
    private LocalDate dateVacationEnd;
    private VacationCountMyDataResponse(VacationCountMyDataResponseBuilder builder) {
        this.vacationTotal = builder.vacationTotal;
        this.vacationCount = builder.vacationCount;
        this.remainCount = builder.remainCount.toString();
        this.dateVacationStart = builder.dateVacationStart;
        this.dateVacationEnd = builder.dateVacationEnd;
    }
    public static class VacationCountMyDataResponseBuilder implements CommonModelBuilder<VacationCountMyDataResponse> {
        private final String vacationTotal;
        private final String vacationCount;
        private final Float remainCount;
        private final LocalDate dateVacationStart;
        private final LocalDate dateVacationEnd;

        public VacationCountMyDataResponseBuilder(VacationCount vacationCount) {
            this.vacationTotal = vacationCount.getVacationTotal().toString();
            this.vacationCount = vacationCount.getVacationCount().toString();
            this.remainCount = vacationCount.getVacationTotal() - vacationCount.getVacationCount();
            this.dateVacationStart = vacationCount.getDateVacationStart();
            this.dateVacationEnd = vacationCount.getDateVacationEnd();
        }

        @Override
        public VacationCountMyDataResponse build() {
            return new VacationCountMyDataResponse(this);
        }
    }
}
