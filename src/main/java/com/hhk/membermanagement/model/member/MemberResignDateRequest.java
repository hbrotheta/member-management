package com.hhk.membermanagement.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * 퇴사일 수정용도 (퇴사처리시 자동으로 퇴사일이 변경되나 담당자가 부재중일시 차후에 퇴사처리하면 퇴사일 수정이 필요함)
 */
@Getter
@Setter
public class MemberResignDateRequest {
    @ApiModelProperty(value = "퇴사일")
    @NotNull
    private LocalDate dateResign;
}
