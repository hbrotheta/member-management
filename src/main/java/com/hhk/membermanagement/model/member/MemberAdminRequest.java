package com.hhk.membermanagement.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

/**
 * 관리자 권한 용도
 */
@Getter
@Setter
public class MemberAdminRequest {
    @ApiModelProperty(value = "관리자 권한")
    @NotNull
    private Boolean isAdmin;
}
