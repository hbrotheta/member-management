package com.hhk.membermanagement.model.member;

import com.hhk.membermanagement.enums.member.Gender;
import com.hhk.membermanagement.enums.member.Position;
import com.hhk.membermanagement.enums.member.Team;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * (관리자) 직원 정보 상세 내역 수정용
 */
@Getter
@Setter
public class MemberInfoDetailsRequest {
    @ApiModelProperty(value = "직원 프로필 사진")
    private String profileUrl;

    @ApiModelProperty(value = "직원 아이디(3~15자)", required = true)
    @NotNull
    @Length(min = 3, max = 15)
    private String username;

    @ApiModelProperty(value = "비밀번호(8~15자)", required = true)
    @NotNull
    @Length(min = 8, max = 15)
    private String password;

    @ApiModelProperty(value = "직원 이름(2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @ApiModelProperty(value = "부서", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Team team;

    @ApiModelProperty(value = "직급", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Position position;

    @ApiModelProperty(value = "성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(value = "직원 연락처(12~20자)", required = true)
    @NotNull
    @Length(min = 12, max = 20)
    private String memberPhone;

    @ApiModelProperty(value = "직원 주소(9~40자)", required = true)
    @NotNull
    @Length(min = 9, max = 40)
    private String memberAddress;

    @ApiModelProperty(value = "입사일", required = true)
    @NotNull
    private LocalDate dateJoin;
}
