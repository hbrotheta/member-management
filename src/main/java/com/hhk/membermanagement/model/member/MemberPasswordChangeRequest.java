package com.hhk.membermanagement.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * (직원) 비밀번호 변경 용도
 */
@Getter
@Setter
public class MemberPasswordChangeRequest {
    @ApiModelProperty(value = "직원 비밀번호 변경", required = true)
    @NotNull
    @Length(min = 8, max = 15)
    private String password;
}
