package com.hhk.membermanagement.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

/**
 * (관리자/직원) 로그인 용도
 */
@Getter
@Setter
public class MemberLoginRequest {
    @ApiModelProperty(value = "직원 아이디", required = true)
    @NotNull
    @Length(min = 3, max = 15)
    private String username;

    @ApiModelProperty(value = "비밀번호", required = true)
    @NotNull
    @Length(min = 8, max = 15)
    private String password;
}
