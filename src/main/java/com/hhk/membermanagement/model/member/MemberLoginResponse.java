package com.hhk.membermanagement.model.member;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * (관리자/직원) 로그인 토큰 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberLoginResponse {
    @ApiModelProperty(value = "직원 시퀀스")
    private Long memberId;
    @ApiModelProperty(value = "직원 아이디")
    private String username;
    @ApiModelProperty(value = "입사일")
    private LocalDate dateJoin;
    private MemberLoginResponse(MemberLoginResponseBuilder builder) {
        this.memberId = builder.memberId;
        this.username = builder.username;
        this.dateJoin = builder.dateJoin;
    }
    public static class MemberLoginResponseBuilder implements CommonModelBuilder<MemberLoginResponse> {
        private final Long memberId;
        private final String username;
        private final LocalDate dateJoin;

        public MemberLoginResponseBuilder(Member member) {
            this.memberId = member.getId();
            this.username = member.getUsername();
            this.dateJoin = member.getDateJoin();
        }

        @Override
        public MemberLoginResponse build() {
            return new MemberLoginResponse(this);
        }
    }
}
