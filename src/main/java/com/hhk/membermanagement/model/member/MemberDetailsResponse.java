package com.hhk.membermanagement.model.member;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.enums.member.Gender;
import com.hhk.membermanagement.enums.member.Position;
import com.hhk.membermanagement.enums.member.Team;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * (관리자) 직원 상세 조회 용도
 */
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberDetailsResponse {
    @ApiModelProperty(value = "시퀀스")
    private Long id;
    @ApiModelProperty(value = "관리자 권한")
    private String isAdmin;
    @ApiModelProperty(value = "직원 사진 주소")
    private String profileUrl;
    @ApiModelProperty(value = "직원 아이디")
    private String username;
    @ApiModelProperty(value = "직원 비밀번호")
    private String password;
    @ApiModelProperty(value = "직원 이름")
    private String memberName;
    @ApiModelProperty(value = "부서명")
    @Enumerated(value = EnumType.STRING)
    private Team team;
    @ApiModelProperty(value = "직원 직급")
    @Enumerated(value = EnumType.STRING)
    private Position position;
    @ApiModelProperty(value = "직원 성별")
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @ApiModelProperty(value = "직원 연락처")
    private String memberPhone;
    @ApiModelProperty(value = "직원 주소")
    private String memberAddress;
    @ApiModelProperty(value = "직원 입사일")
    private LocalDate dateJoin;
    @ApiModelProperty(value = "직원 퇴사일")
    private LocalDate dateResign;
    @ApiModelProperty(value = "근로 유/무")
    private String isWorking;
    @ApiModelProperty(value = "데이터 등록시간")
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "데이터 수정시간")
    private LocalDateTime dateUpdate;
    private MemberDetailsResponse(MemberInfoDetailsResponseBuilder builder) {
        this.id = builder.id;
        this.isAdmin = builder.isAdmin;
        this.profileUrl = builder.profileUrl;
        this.username = builder.username;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.team = builder.team;
        this.position = builder.position;
        this.gender = builder.gender;
        this.memberPhone = builder.memberPhone;
        this.memberAddress = builder.memberAddress;
        this.dateJoin = builder.dateJoin;
        this.dateResign = builder.dateResign;
        this.isWorking = builder.isWorking;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class MemberInfoDetailsResponseBuilder implements CommonModelBuilder<MemberDetailsResponse> {
        private final Long id;
        private final String isAdmin;
        private final String profileUrl;
        private final String username;
        private final String password;
        private final String memberName;
        private final Team team;
        private final Position position;
        private final Gender gender;
        private final String memberPhone;
        private final String memberAddress;
        private final LocalDate dateJoin;
        private final LocalDate dateResign;
        private final String isWorking;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        public MemberInfoDetailsResponseBuilder(Member member) {
            this.id = member.getId();
            this.isAdmin = member.getIsAdmin() ? member.getIsAdmin() + "관리자" : "일반직원";
            this.profileUrl = member.getProfileUrl();
            this.username = member.getUsername();
            this.password = member.getPassword();
            this.memberName = member.getMemberName();
            this.team = member.getTeam();
            this.position = member.getPosition();
            this.gender = member.getGender();
            this.memberPhone = member.getMemberPhone();
            this.memberAddress = member.getMemberAddress();
            this.dateJoin = member.getDateJoin();
            this.dateResign = member.getDateResign();
            this.isWorking = member.getIsWorking() ? member.getIsWorking() + "근무자" : "퇴사자";
            this.dateCreate = member.getDateCreate();
            this.dateUpdate = member.getDateUpdate();
        }
        @Override
        public MemberDetailsResponse build() {
            return new MemberDetailsResponse(this);
        }
    }
}
