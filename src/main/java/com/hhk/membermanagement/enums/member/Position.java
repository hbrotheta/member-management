package com.hhk.membermanagement.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 직급 종류 enum
 */
@Getter
@AllArgsConstructor
public enum Position {
    CEO("최고책임자")
    , CHIEF("수석")
    , SENIOR("선임")
    , ASSISTANT("주임")
    ;

    private final String name;
}
