package com.hhk.membermanagement.enums.member;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 성별 enum
 */
@Getter
@AllArgsConstructor
public enum Gender {
    MAN("남자")
    , WOMAN("여자")
    ;

    private final String name;
}
