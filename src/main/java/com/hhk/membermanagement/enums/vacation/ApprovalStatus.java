package com.hhk.membermanagement.enums.vacation;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 승인 상태 enum
 */
@Getter
@AllArgsConstructor
public enum ApprovalStatus {
    APPROVAL("승인")
    , APPROVING("대기중")
    , REFUSAL("반려")
    ;

    private final String name;
}
