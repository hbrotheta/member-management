package com.hhk.membermanagement.enums.vacation;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 휴가 신청 종류 enum
 */
@Getter
@AllArgsConstructor
public enum VacationType {
    ANNUAL_LEAVE("연차")
    , SICK_LEAVE("병가")
    , ANNUAL_LEAVE_ADD("특별 휴가")
    , ETC("기타")
    ;

    private final String name;
}
