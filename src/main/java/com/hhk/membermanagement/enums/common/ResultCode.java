package com.hhk.membermanagement.enums.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 예외처리 enum
 */
@Getter
@AllArgsConstructor
public enum ResultCode {
    SUCCESS(0,"성공하였습니다.")
    , FAILED(-1, "실패하였습니다.")

    , MISSING_DATA(-10000, "데이터를 찾을 수 없습니다.")

    , USERNAME_USING(-20001, "아이디가 사용중 입니다.")

    , LOGIN_DATA(30000, "로그인 하였습니다.")
    , LOGIN_PASSWORD_FAILED(-30001, "비밀번호가 틀렸습니다.")
    , LOGIN_MEMBER_RESIGN(-30005, "퇴사 직원 입니다. 관리자에게 문의 바랍니다.")

    , DAILY_CHECK_ALREADY_WORK_START(-40000, "이미 출근 하였습니다.")
    , DAILY_CHECK_MISSING_WORK_START(-40001, "출근 기록을 찾을 수 없습니다.")
    , DAILY_CHECK_CHANGE_WORK_START(-40002, "출근 상태로 다시 변경 할 수 없습니다.")
    , DAILY_CHECK_NOT_CHANGE_WORK_END(-40003, "퇴근후에는 다시 할 수 없습니다.")
    , DAILY_CHECK_STATE_SAME(-40010, "동일한 상태로 다시 할 수 없습니다.")

    , VACATION_APPROVAL_STATE_ALREADY(-50001, "이미 처리된 상태입니다.")
    , VACATION_APPROVING_SAME(50002, "이미 대기중 입니다.")
    , HAVE_NOT_VACATION_COUNT(-50003, "연차 수량이 부족 합니다.")
    , REFUSAL_VACATION_COUNT(-50004, "연차 승인이 반려 되었습니다.")
    , HAVE_NOT_VACATION_COUNT_REFUSAL(-50005, "연차 수량이 부족 하여 자동 반려 되었습니다.")

    ;

    private final Integer code;
    private final String msg;
}
