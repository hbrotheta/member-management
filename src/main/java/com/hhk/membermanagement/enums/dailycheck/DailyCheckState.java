package com.hhk.membermanagement.enums.dailycheck;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 출퇴근 종류 enum
 */
@Getter
@AllArgsConstructor
public enum DailyCheckState {
    WORK_START("출근")
    , SHORT_OUTING("외출")
    , COMEBACK("복귀")
    , WORK_END("퇴근")
    , UNKNOWN("알수없음")
    ;

    private final String name;
}
