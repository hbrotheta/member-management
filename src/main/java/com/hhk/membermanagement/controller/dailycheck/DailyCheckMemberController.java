package com.hhk.membermanagement.controller.dailycheck;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.enums.dailycheck.DailyCheckState;
import com.hhk.membermanagement.model.common.ListResult;
import com.hhk.membermanagement.model.common.SingleResult;
import com.hhk.membermanagement.model.dailycheck.DailyCheckMyDataResponse;
import com.hhk.membermanagement.model.dailycheck.DailyCheckResponse;
import com.hhk.membermanagement.service.common.ResponseService;
import com.hhk.membermanagement.service.dailycheck.DailyCheckMemberService;
import com.hhk.membermanagement.service.member.MemberService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@Api(tags = "근태 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/daily-check")
public class DailyCheckMemberController {
    private final MemberService memberService;
    private final DailyCheckMemberService dailyCheckMemberService;
    @ApiOperation(value = "출퇴근 상태 가져오기")
    @GetMapping("/state/member-id/{memberId}")
    public SingleResult<DailyCheckResponse> getStatus(@PathVariable long memberId) {
        Member member = memberService.getMyData(memberId);
        return ResponseService.getSingleResult(dailyCheckMemberService.getCurrentState(member));
    }

    @ApiOperation(value = "근태 상태 수정")
    @PutMapping("/state/{dailyCheckState}/member-id/{memberId}")
    public SingleResult<DailyCheckResponse> doStateChange(
            @PathVariable DailyCheckState dailyCheckState,
            @PathVariable long memberId
    ) {
        Member member = memberService.getMyData(memberId);
        return ResponseService.getSingleResult(dailyCheckMemberService.doDailyCheckChange(member, dailyCheckState));
    }

    @ApiOperation(value = "나의 근태 리스트 가져오기")
    @GetMapping("/my-data/{memberId}")
    public ListResult<DailyCheckMyDataResponse> getDailyCheckMyData(@PathVariable long memberId) {
        return ResponseService.getListResult(dailyCheckMemberService.getDailyCheckMyData(memberId),true);
    }
}
