package com.hhk.membermanagement.controller.vacation;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.entity.VacationTotalUsage;
import com.hhk.membermanagement.enums.vacation.ApprovalStatus;
import com.hhk.membermanagement.model.common.CommonResult;
import com.hhk.membermanagement.model.common.ListResult;
import com.hhk.membermanagement.model.common.SingleResult;
import com.hhk.membermanagement.model.vacation.VacationApplyItem;
import com.hhk.membermanagement.model.vacation.VacationCountMyDataResponse;
import com.hhk.membermanagement.model.vacation.VacationDefaultCountRequest;
import com.hhk.membermanagement.model.vacation.VacationMemberRequest;
import com.hhk.membermanagement.service.common.ResponseService;
import com.hhk.membermanagement.service.member.MemberService;
import com.hhk.membermanagement.service.vacation.VacationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "휴가 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/vacation")
public class VacationController {
    private final VacationService vacationService;
    private final MemberService memberService;
    @ApiOperation(value = "기본 연차 수량 부여")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @PutMapping("/default-count/{memberId}")
    public CommonResult putVacationDefaultCount(@PathVariable long memberId, @RequestBody @Valid VacationDefaultCountRequest request) {
        vacationService.putVacationDefaultCount(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "연차 수량 정보 가져오기")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @GetMapping("/my-count/{memberId}")
    public SingleResult<VacationCountMyDataResponse> getMyVacationCount(@PathVariable long memberId) {
        return ResponseService.getSingleResult(vacationService.getMyVacationCount(memberId));
    }

    @ApiOperation(value = "휴가 신청 등록")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @PostMapping("/my-apply/{memberId}")
    public CommonResult setMemberRequestVacation(@PathVariable long memberId, @RequestBody @Valid VacationMemberRequest Request) {
        Member member = memberService.getMyData(memberId);
        vacationService.setMemberRequestVacation(member, Request);
        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "해당 직원 휴가 리스트 가져오기")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @GetMapping("/my-apply-list/{memberId}")
    public ListResult<VacationApplyItem> getMyVacationApplies(@PathVariable long memberId) {
            return ResponseService.getListResult(vacationService.getMyVacationApplies(memberId),true);
    }

    @ApiOperation(value = "전체 직원 휴가 리스트 가져오기")
    @GetMapping("/all-apply-list")
    public ListResult<VacationApplyItem> getVacationApplies() {
        return ResponseService.getListResult(vacationService.getVacationApplies(),true);
    }

    @ApiOperation(value = "휴가 리스트 가져오기(승인, 대기, 반려)")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "approvalStatus", value = "승인, 대기, 반려", required = true)
    )
    @GetMapping("/approval-status")
    public ListResult<VacationApplyItem> getVacationApplyListFilter(@RequestParam ApprovalStatus approvalStatus) {
        return ResponseService.getListResult(vacationService.getVacationApplyListFilter(approvalStatus), true);
    }
    @ApiOperation(value = "휴가 신청 처리 및 연차 수량 변경")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "vacationTotalUsageId", value = "휴가종합내역 시퀀스", required = true)
    )
    @PutMapping("/approval/{vacationTotalUsageId}")
    public CommonResult putVacationApproval(@PathVariable long vacationTotalUsageId, @RequestParam ApprovalStatus approvalStatus) {
        VacationTotalUsage vacationTotalUsage = vacationService.getVacationApprovingData(vacationTotalUsageId);
        vacationService.putVacationApproval(vacationTotalUsage, approvalStatus);
        return ResponseService.getSuccessResult();
    }
}
