package com.hhk.membermanagement.controller.member;

import com.hhk.membermanagement.model.common.SingleResult;
import com.hhk.membermanagement.model.member.MemberLoginRequest;
import com.hhk.membermanagement.model.member.MemberLoginResponse;
import com.hhk.membermanagement.service.common.ResponseService;
import com.hhk.membermanagement.service.member.MemberLoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "관리자 로그인 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login-admin")
public class AdminLoginController {
    private final MemberLoginService memberLoginService;
    @ApiOperation(value = "관리자 로그인 및 기본 데이터 가져오기")
    @GetMapping("/default-data")
    public SingleResult<MemberLoginResponse> getMemberLogin(@RequestBody @Valid MemberLoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberLoginService.getMemberLogin(true, loginRequest));
    }
}
