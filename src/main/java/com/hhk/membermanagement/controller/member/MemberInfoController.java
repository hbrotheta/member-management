package com.hhk.membermanagement.controller.member;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.model.common.CommonResult;
import com.hhk.membermanagement.model.common.ListResult;
import com.hhk.membermanagement.model.common.SingleResult;
import com.hhk.membermanagement.model.member.MemberAdminRequest;
import com.hhk.membermanagement.model.member.MemberInfoDetailsRequest;
import com.hhk.membermanagement.model.member.MemberDetailsResponse;
import com.hhk.membermanagement.model.member.MemberResignDateRequest;
import com.hhk.membermanagement.service.member.MemberService;
import com.hhk.membermanagement.service.common.ResponseService;
import com.hhk.membermanagement.service.vacation.VacationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "직원 정보 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-info")
public class MemberInfoController {
    private final MemberService memberService;
    private final VacationService vacationService;
    // 등록하자 마자 연차등록 확인해 봐야함
    @ApiOperation(value = "직원 정보 등록 시 연차 정보 자동 등록")
    @PostMapping("/join")
    public CommonResult setMember(@RequestBody @Valid MemberInfoDetailsRequest request) {
        Member member = memberService.setMember(request);
        vacationService.setDefaultVacation(member.getId(), member.getDateJoin());
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 정보 가져오기")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @GetMapping("/{memberId}")
    public SingleResult<MemberDetailsResponse> getMember(@PathVariable long memberId) {
        return ResponseService.getSingleResult(memberService.getMember(memberId));
    }

    @ApiOperation(value = "직원 정보 리스트 가져오기")
    @GetMapping("/data")
    public ListResult<MemberDetailsResponse> getMembers() {
        return ResponseService.getListResult(memberService.getMembers(), true);
    }

    @ApiOperation(value = "직원 정보 수정하기")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @PutMapping("/{memberId}")
    public CommonResult putMember(@PathVariable long memberId, @RequestBody @Valid MemberInfoDetailsRequest request) {
        memberService.putMember(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "관리자 권한 수정하기")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @PutMapping("isAdmin/{memberId}")
    public CommonResult putMemberAdmin(@PathVariable long memberId, @RequestBody @Valid MemberAdminRequest request) {
        memberService.putMemberAdmin(memberId, request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "직원 퇴사일 수정하기")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true)
    )
    @DeleteMapping("resign/{memberId}")
    public CommonResult putMemberResign(@PathVariable long memberId, @RequestBody @Valid MemberResignDateRequest request) {
        memberService.putMemberResign(memberId, request);
        return ResponseService.getSuccessResult();
    }
}
