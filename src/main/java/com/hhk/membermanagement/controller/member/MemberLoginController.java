package com.hhk.membermanagement.controller.member;

import com.hhk.membermanagement.model.common.SingleResult;
import com.hhk.membermanagement.model.member.MemberLoginRequest;
import com.hhk.membermanagement.model.member.MemberLoginResponse;
import com.hhk.membermanagement.service.common.ResponseService;
import com.hhk.membermanagement.service.member.MemberLoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "일반 직원 로그인 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/login-member")
public class MemberLoginController {
    private final MemberLoginService memberLoginService;
    @ApiOperation(value = "일반직원 로그인 및 기본 데이터 가져오기")
    @PostMapping("/default-data")
    public SingleResult<MemberLoginResponse> getMemberLogin(@RequestBody @Valid MemberLoginRequest loginRequest) {
        return ResponseService.getSingleResult(memberLoginService.getMemberLogin(false, loginRequest));
    }
}
