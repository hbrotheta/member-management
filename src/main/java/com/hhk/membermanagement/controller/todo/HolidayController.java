package com.hhk.membermanagement.controller.todo;

import com.hhk.membermanagement.entity.Member;
import com.hhk.membermanagement.model.common.CommonResult;
import com.hhk.membermanagement.model.common.SingleResult;
import com.hhk.membermanagement.model.todo.MyHolidayCountResponse;
import com.hhk.membermanagement.service.common.ResponseService;
import com.hhk.membermanagement.service.todo.HolidayService;
import com.hhk.membermanagement.service.member.MemberService;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Api(tags = "연차 관리 API")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/holiday")
public class HolidayController {
    private final HolidayService holidayService;
    private final MemberService memberService;

    @ApiOperation(value = "연차 초기 등록")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true),
            @ApiImplicitParam(name = "dateCriteria", value = "기준일", required = true)
    })
    @PostMapping("/default-count/{memberId}")
    public CommonResult setDefaultCount(@PathVariable long memberId, @RequestParam("dateCriteria") LocalDate dateCriteria) {
        Member member = memberService.getMyData(memberId);
        holidayService.setDefaultCount(member, dateCriteria);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "연차 정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "memberId", value = "직원 시퀀스", required = true),
            @ApiImplicitParam(name = "dateCriteria", value = "기준일", required = true)
    })
    @GetMapping("/my-count/{memberId}")
    public SingleResult<MyHolidayCountResponse> getMyCount(
            @PathVariable long memberId,
            @RequestParam("dateCriteria") LocalDate dateCriteria) {
        Member member = memberService.getMyData(memberId);
        return ResponseService.getSingleResult(holidayService.getMyCount(member, dateCriteria));
    }
}
