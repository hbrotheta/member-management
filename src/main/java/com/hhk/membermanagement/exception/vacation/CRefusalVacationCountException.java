package com.hhk.membermanagement.exception.vacation;

public class CRefusalVacationCountException extends RuntimeException {
    public CRefusalVacationCountException(String msg, Throwable t) {
        super(msg, t);
    }

    public CRefusalVacationCountException(String msg) {
        super(msg);
    }

    public CRefusalVacationCountException() {
        super();
    }
}
