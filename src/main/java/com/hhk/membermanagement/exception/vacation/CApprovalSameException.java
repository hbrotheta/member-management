package com.hhk.membermanagement.exception.vacation;

public class CApprovalSameException extends RuntimeException {
    public CApprovalSameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CApprovalSameException(String msg) {
        super(msg);
    }

    public CApprovalSameException() {
        super();
    }
}
