package com.hhk.membermanagement.exception.vacation;

public class CApprovalStateAlreadyException extends RuntimeException {
    public CApprovalStateAlreadyException(String msg, Throwable t) {
        super(msg, t);
    }

    public CApprovalStateAlreadyException(String msg) {
        super(msg);
    }

    public CApprovalStateAlreadyException() {
        super();
    }
}
