package com.hhk.membermanagement.exception.vacation;

public class CVacationCountException extends RuntimeException {
    public CVacationCountException(String msg, Throwable t) {
        super(msg, t);
    }

    public CVacationCountException(String msg) {
        super(msg);
    }

    public CVacationCountException() {
        super();
    }
}
