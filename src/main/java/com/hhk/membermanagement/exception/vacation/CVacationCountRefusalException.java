package com.hhk.membermanagement.exception.vacation;

public class CVacationCountRefusalException extends RuntimeException {
    public CVacationCountRefusalException(String msg, Throwable t) {
        super(msg, t);
    }

    public CVacationCountRefusalException(String msg) {
        super(msg);
    }

    public CVacationCountRefusalException() {
        super();
    }
}
