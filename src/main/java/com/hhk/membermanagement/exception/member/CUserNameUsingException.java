package com.hhk.membermanagement.exception.member;

public class CUserNameUsingException extends RuntimeException {
    public CUserNameUsingException(String msg, Throwable t) {
        super(msg, t);
    }

    public CUserNameUsingException(String msg) {
        super(msg);
    }

    public CUserNameUsingException() {
        super();
    }
}