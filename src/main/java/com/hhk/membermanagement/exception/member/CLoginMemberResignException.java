package com.hhk.membermanagement.exception.member;

public class CLoginMemberResignException extends RuntimeException {
    public CLoginMemberResignException(String msg, Throwable t) {
        super(msg, t);
    }

    public CLoginMemberResignException(String msg) {
        super(msg);
    }

    public CLoginMemberResignException() {
        super();
    }
}