package com.hhk.membermanagement.exception.dailycheck;

public class CDailyCheckChangeWorkEndException extends RuntimeException {
    public CDailyCheckChangeWorkEndException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDailyCheckChangeWorkEndException(String msg) {
        super(msg);
    }

    public CDailyCheckChangeWorkEndException() {
        super();
    }
}
