package com.hhk.membermanagement.exception.dailycheck;

public class CDailyCheckAlreadyWorkStartException extends RuntimeException {
    public CDailyCheckAlreadyWorkStartException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDailyCheckAlreadyWorkStartException(String msg) {
        super(msg);
    }

    public CDailyCheckAlreadyWorkStartException() {
        super();
    }
}
