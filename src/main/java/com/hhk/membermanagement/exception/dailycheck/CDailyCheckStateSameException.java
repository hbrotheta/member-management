package com.hhk.membermanagement.exception.dailycheck;

public class CDailyCheckStateSameException extends RuntimeException {
    public CDailyCheckStateSameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDailyCheckStateSameException(String msg) {
        super(msg);
    }

    public CDailyCheckStateSameException() {
        super();
    }
}
