package com.hhk.membermanagement.exception.dailycheck;

public class CDailyCheckNotChangeWorkEndException extends RuntimeException {
    public CDailyCheckNotChangeWorkEndException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDailyCheckNotChangeWorkEndException(String msg) {
        super(msg);
    }

    public CDailyCheckNotChangeWorkEndException() {
        super();
    }
}
