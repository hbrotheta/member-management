package com.hhk.membermanagement.exception.dailycheck;

public class CDailyCheckMissingWorkStartException extends RuntimeException {
    public CDailyCheckMissingWorkStartException(String msg, Throwable t) {
        super(msg, t);
    }

    public CDailyCheckMissingWorkStartException(String msg) {
        super(msg);
    }

    public CDailyCheckMissingWorkStartException() {
        super();
    }
}
