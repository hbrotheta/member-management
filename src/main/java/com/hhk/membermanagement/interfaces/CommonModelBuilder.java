package com.hhk.membermanagement.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
