package com.hhk.membermanagement.advice;

import com.hhk.membermanagement.enums.common.ResultCode;
import com.hhk.membermanagement.exception.common.CMissingDataException;
import com.hhk.membermanagement.exception.dailycheck.*;
import com.hhk.membermanagement.exception.member.CLoginMemberResignException;
import com.hhk.membermanagement.exception.member.CLoginPasswordFailedException;
import com.hhk.membermanagement.exception.member.CUserNameUsingException;
import com.hhk.membermanagement.exception.vacation.*;
import com.hhk.membermanagement.model.common.CommonResult;
import com.hhk.membermanagement.service.common.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }
    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return  ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }
    @ExceptionHandler(CLoginPasswordFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CLoginPasswordFailedException e) {
        return  ResponseService.getFailResult(ResultCode.LOGIN_PASSWORD_FAILED);
    }
    @ExceptionHandler(CLoginMemberResignException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CLoginMemberResignException e) {
        return  ResponseService.getFailResult(ResultCode.LOGIN_MEMBER_RESIGN);
    }
    @ExceptionHandler(CDailyCheckAlreadyWorkStartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDailyCheckAlreadyWorkStartException e) {
        return  ResponseService.getFailResult(ResultCode.DAILY_CHECK_ALREADY_WORK_START);
    }
    @ExceptionHandler(CDailyCheckMissingWorkStartException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDailyCheckMissingWorkStartException e) {
        return  ResponseService.getFailResult(ResultCode.DAILY_CHECK_MISSING_WORK_START);
    }
    @ExceptionHandler(CDailyCheckNotChangeWorkEndException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDailyCheckNotChangeWorkEndException e) {
        return  ResponseService.getFailResult(ResultCode.DAILY_CHECK_NOT_CHANGE_WORK_END);
    }
    @ExceptionHandler(CDailyCheckChangeWorkEndException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDailyCheckChangeWorkEndException e) {
        return  ResponseService.getFailResult(ResultCode.DAILY_CHECK_CHANGE_WORK_START);
    }
    @ExceptionHandler(CDailyCheckStateSameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CDailyCheckStateSameException e) {
        return  ResponseService.getFailResult(ResultCode.DAILY_CHECK_STATE_SAME);
    }
    @ExceptionHandler(CUserNameUsingException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CUserNameUsingException e) {
        return  ResponseService.getFailResult(ResultCode.USERNAME_USING);
    }
    @ExceptionHandler(CApprovalStateAlreadyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApprovalStateAlreadyException e) {
        return  ResponseService.getFailResult(ResultCode.VACATION_APPROVAL_STATE_ALREADY);
    }
    @ExceptionHandler(CApprovalSameException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CApprovalSameException e) {
        return  ResponseService.getFailResult(ResultCode.VACATION_APPROVING_SAME);
    }
    @ExceptionHandler(CVacationCountException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CVacationCountException e) {
        return  ResponseService.getFailResult(ResultCode.HAVE_NOT_VACATION_COUNT);
    }
    @ExceptionHandler(CRefusalVacationCountException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CRefusalVacationCountException e) {
        return  ResponseService.getFailResult(ResultCode.REFUSAL_VACATION_COUNT);
    }
    @ExceptionHandler(CVacationCountRefusalException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CVacationCountRefusalException e) {
        return  ResponseService.getFailResult(ResultCode.HAVE_NOT_VACATION_COUNT_REFUSAL);
    }
}
