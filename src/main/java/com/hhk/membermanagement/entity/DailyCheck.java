package com.hhk.membermanagement.entity;

import com.hhk.membermanagement.enums.dailycheck.DailyCheckState;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * 출퇴근 관리
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DailyCheck {
    @ApiModelProperty(value = "시퀀스", required = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "직원", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;
    @ApiModelProperty(value = "출/외/복/퇴/알수없음 상태(최대 20자)", required = true)
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private DailyCheckState dailyCheckState;
    @ApiModelProperty(value = "기준일", required = true)
    @Column(nullable = false)
    private LocalDate dateBase;
    @ApiModelProperty(value = "출근 시간", required = true)
    @Column(nullable = false)
    private LocalTime dateWorkStart;
    @ApiModelProperty(value = "외출 시간")
    private LocalTime dateShortOuting;
    @ApiModelProperty(value = "복귀 시간")
    private LocalTime dateWorkComeBack;
    @ApiModelProperty(value = "퇴근 시간")
    private LocalTime dateWorkEnd;
    public void putDailyCheckState(DailyCheckState dailyCheckState) {
        this.dailyCheckState = dailyCheckState;

        switch (dailyCheckState) {
            case SHORT_OUTING:
                this.dateShortOuting = LocalTime.now();
                break;
            case COMEBACK:
                this.dateWorkComeBack = LocalTime.now();
                break;
            case WORK_END:
                this.dateWorkEnd = LocalTime.now();
                break;
        }
    }
    private DailyCheck(DailyCheckBuilder builder) {
        this.member = builder.member;
        this.dailyCheckState = builder.dailyCheckState;
        this.dateBase = builder.dateBase;
        this.dateWorkStart = builder.dateWorkStart;
    }
    public static class DailyCheckBuilder implements CommonModelBuilder<DailyCheck> {
        private final Member member;
        private final DailyCheckState dailyCheckState;
        private final LocalDate dateBase;
        private final LocalTime dateWorkStart;
        // 출근 버튼 활성화시
        public DailyCheckBuilder(Member member) {
            this.member = member;
            this.dailyCheckState = DailyCheckState.WORK_START;
            this.dateBase = LocalDate.now();
            this.dateWorkStart = LocalTime.now();
        }
        @Override
        public DailyCheck build() {
            return new DailyCheck(this);
        }
    }
    private DailyCheck(DailyCheckUnknownBuilder builder) {
        this.dailyCheckState = builder.dailyCheckState;
    }
    public static class DailyCheckUnknownBuilder implements CommonModelBuilder<DailyCheck> {
        private final DailyCheckState dailyCheckState;
        // 알수없음 상태
        public DailyCheckUnknownBuilder() {
            this.dailyCheckState = DailyCheckState.UNKNOWN;
        }
        @Override
        public DailyCheck build() {
            return new DailyCheck(this);
        }
    }
}
