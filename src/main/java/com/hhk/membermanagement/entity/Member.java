package com.hhk.membermanagement.entity;

import com.hhk.membermanagement.enums.member.Gender;
import com.hhk.membermanagement.enums.member.Position;
import com.hhk.membermanagement.enums.member.Team;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import com.hhk.membermanagement.model.member.MemberAdminRequest;
import com.hhk.membermanagement.model.member.MemberInfoDetailsRequest;
import com.hhk.membermanagement.model.member.MemberResignDateRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 직원 정보
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @ApiModelProperty(value = "시퀀스", required = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "관리자 권한", required = true)
    @Column(nullable = false)
    private Boolean isAdmin;
    @ApiModelProperty(value = "직원 사진 주소")
    private String profileUrl;
    @ApiModelProperty(value = "직원 아이디(최대 15자)", required = true)
    @Column(nullable = false, length = 15, unique = true)
    private String username;
    @ApiModelProperty(value = "직원 비밀번호(최대 15자)", required = true)
    @Column(nullable = false, length = 15)
    private String password;
    @ApiModelProperty(value = "직원 이름(최대 20자)", required = true)
    @Column(nullable = false, length = 20)
    private String memberName;
    @ApiModelProperty(value = "부서명(최대 30자)", required = true)
    @Column(nullable = false, length = 30)
    @Enumerated(value = EnumType.STRING)
    private Team team;
    @ApiModelProperty(value = "직원 직급(최대 15자)", required = true)
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Position position;
    @ApiModelProperty(value = "직원 성별(최대 15자)", required = true)
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;
    @ApiModelProperty(value = "직원 연락처(최대 20자)", required = true)
    @Column(nullable = false, length = 20)
    private String memberPhone;
    @ApiModelProperty(value = "직원 주소(최대 40자)", required = true)
    @Column(nullable = false, length = 40)
    private String memberAddress;
    @ApiModelProperty(value = "직원 입사일", required = true)
    @Column(nullable = false)
    private LocalDate dateJoin;
    @ApiModelProperty(value = "직원 퇴사일")
    private LocalDate dateResign;
    @ApiModelProperty(value = "근로 유/무", required = true)
    @Column(nullable = false)
    private Boolean isWorking;
    @ApiModelProperty(value = "데이터 등록시간", required = true)
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "데이터 수정시간", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putMemberAdmin(MemberAdminRequest request) {
        this.isAdmin = request.getIsAdmin();
        this.dateUpdate = LocalDateTime.now();
    }
    public void putMemberResign(MemberResignDateRequest request) {
        this.isAdmin = false;
        this.isWorking = false;
        this.dateResign = request.getDateResign();
        this.dateUpdate = LocalDateTime.now();
    }
    public void putMember(MemberInfoDetailsRequest request) {
        this.profileUrl = request.getProfileUrl();
        this.username = request.getUsername();
        this.password = request.getPassword();
        this.memberName = request.getMemberName();
        this.team = request.getTeam();
        this.position = request.getPosition();
        this.gender = request.getGender();
        this.memberPhone = request.getMemberPhone();
        this.memberAddress = request.getMemberAddress();
        this.dateJoin = request.getDateJoin();
        this.dateUpdate = LocalDateTime.now();
    }
    private Member(MemberBuilder builder) {
        this.isAdmin = builder.isAdmin;
        this.profileUrl = builder.profileUrl;
        this.username = builder.username;
        this.password = builder.password;
        this.memberName = builder.memberName;
        this.team = builder.team;
        this.position = builder.position;
        this.gender = builder.gender;
        this.memberPhone = builder.memberPhone;
        this.memberAddress = builder.memberAddress;
        this.dateJoin = builder.dateJoin;
        this.dateResign = builder.dateResign;
        this.isWorking = builder.isWorking;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final Boolean isAdmin;
        private final String profileUrl;
        private final String username;
        private final String password;
        private final String memberName;
        private final Team team;
        private final Position position;
        private final Gender gender;
        private final String memberPhone;
        private final String memberAddress;
        private final LocalDate dateJoin;
        private final LocalDate dateResign;
        private final Boolean isWorking;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        public MemberBuilder(MemberInfoDetailsRequest request) {
            this.isAdmin = false;
            this.profileUrl = request.getProfileUrl();
            this.username = request.getUsername();
            this.password = request.getPassword();
            this.memberName = request.getMemberName();
            this.team = request.getTeam();
            this.position = request.getPosition();
            this.gender = request.getGender();
            this.memberPhone = request.getMemberPhone();
            this.memberAddress = request.getMemberAddress();
            this.dateJoin = request.getDateJoin();
            this.dateResign = null;
            this.isWorking = true;
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
