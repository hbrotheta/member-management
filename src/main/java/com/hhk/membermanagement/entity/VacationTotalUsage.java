package com.hhk.membermanagement.entity;

import com.hhk.membermanagement.enums.vacation.ApprovalStatus;
import com.hhk.membermanagement.enums.vacation.VacationType;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import com.hhk.membermanagement.model.vacation.VacationMemberRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 휴가 사용 내역
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationTotalUsage {
    @ApiModelProperty(value = "시퀀스", required = true)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "직원", required = true)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;
    @ApiModelProperty(value = "휴가 종류", required = true)
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private VacationType vacationType;
    @ApiModelProperty(value = "휴가 시작일", required = true)
    @Column(nullable = false)
    private LocalDate vacationStart;
    @ApiModelProperty(value = "휴가 종료일", required = true)
    @Column(nullable = false)
    private LocalDate vacationEnd;
    @ApiModelProperty(value = "휴가 사유(최대 20자)", required = true)
    @Column(nullable = false, length = 20)
    private String vacationReason;
    @ApiModelProperty(value = "승인 상태", required = true)
    @Column(nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    private ApprovalStatus approvalStatus;
    @ApiModelProperty(notes = "직원 휴가 요청값")
    @Column(nullable = false)
    private Float vacationApplyValue;
    @ApiModelProperty(notes = "관리자 실제 휴가 반영값")
    @Column(nullable = false)
    private Float fixIncreaseOrDecreaseValue;
    @ApiModelProperty(value = "관리자 승인 시간")
    private LocalDateTime dateApproval;
    @ApiModelProperty(value = "관리자 반려 시간")
    private LocalDateTime dateRefusal;
    @ApiModelProperty(value = "휴가 등록시간", required = true)
    @Column(nullable = false)
    private LocalDateTime dateApproving;
    @ApiModelProperty(value = "데이터 수정시간", required = true)
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    // 휴가 승인/반려 처리 메서드
    public void putVacationApproval(Float vacationApplyValue, ApprovalStatus approvalStatus) {
        switch (approvalStatus) {
            case APPROVAL:
                this.approvalStatus = approvalStatus;
                this.fixIncreaseOrDecreaseValue = vacationApplyValue;
                this.dateApproval = LocalDateTime.now();
                break;
            case REFUSAL:
                this.approvalStatus = approvalStatus;
                this.fixIncreaseOrDecreaseValue = 0f;
                this.dateRefusal = LocalDateTime.now();
                break;
        }
    }
    private VacationTotalUsage(VacationApplyBuilder builder) {
        this.member = builder.member;
        this.vacationType = builder.vacationType;
        this.vacationStart = builder.vacationStart;
        this.vacationEnd = builder.vacationEnd;
        this.vacationReason = builder.vacationReason;
        this.approvalStatus = builder.approvalStatus;
        this.vacationApplyValue = builder.vacationApplyValue;
        this.fixIncreaseOrDecreaseValue = builder.fixIncreaseOrDecreaseValue;
        this.dateApproving = builder.dateApproving;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class VacationApplyBuilder implements CommonModelBuilder<VacationTotalUsage> {
        private final Member member;
        private final VacationType vacationType;
        private final LocalDate vacationStart;
        private final LocalDate vacationEnd;
        private final String vacationReason;
        private final ApprovalStatus approvalStatus;
        private final Float vacationApplyValue;
        private final Float fixIncreaseOrDecreaseValue;
        private final LocalDateTime dateApproving;
        private final LocalDateTime dateUpdate;

        public VacationApplyBuilder(Member member, VacationMemberRequest memberRequest) {
            this.member = member;
            this.vacationType = memberRequest.getVacationType();
            this.vacationStart = memberRequest.getVacationStart();
            this.vacationEnd = memberRequest.getVacationEnd();
            this.vacationReason = memberRequest.getVacationReason();
            this.approvalStatus = ApprovalStatus.APPROVING;
            this.vacationApplyValue = memberRequest.getIncreaseOrDecreaseValue();
            this.fixIncreaseOrDecreaseValue = 0f;
            this.dateApproving = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public VacationTotalUsage build() {
            return new VacationTotalUsage(this);
        }
    }
}

