package com.hhk.membermanagement.entity;

import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import com.hhk.membermanagement.model.vacation.VacationDefaultCountRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * 연차 수량
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class VacationCount {
    // Member 랑 원투원으로 조인시키지 않은 이유는 직원 테이블은 항상 많은 참조를 하게되는데
    // 그때마다 휴가 카운트를 매칭하여 달고 다니는 것은 좋지않으며, 유지보수 또한 매우힘들어진다.
    // 직원 정보안에서도 특별하게 쓰일 데이터라면 테이블 분리를 하는게 좋다.
    @ApiModelProperty(value = "시퀀스")
    @Id
    private Long memberId;
    @ApiModelProperty(value = "총 휴가 수량")
    @Column(nullable = false)
    private Float vacationTotal;
    @ApiModelProperty(value = "사용한 연차 수량")
    @Column(nullable = false)
    private Float vacationCount;
    @ApiModelProperty(value = "연차 유효 기간 시작일")
    @Column(nullable = false)
    private LocalDate dateVacationStart;
    @ApiModelProperty(value = "연차 유효 기간 종료일")
    @Column(nullable = false)
    private LocalDate dateVacationEnd;
    @ApiModelProperty(value = "등록시간")
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "수정시간")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putVacationDefaultCount(VacationDefaultCountRequest Request) {
        this.vacationTotal = Request.getVacationTotal();
        this.dateVacationEnd = Request.getDateVacationStart().plusYears(1).minusDays(1);
        this.dateUpdate = LocalDateTime.now();
    }

    public void plusVacationCount(float plusCount) {
        this.vacationCount += plusCount;
    }
    private VacationCount(VacationBuilder builder) {
        this.memberId = builder.memberId;
        this.vacationTotal = builder.vacationTotal;
        this.vacationCount = builder.vacationCount;
        this.dateVacationStart = builder.dateVacationStart;
        this.dateVacationEnd = builder.dateVacationEnd;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class VacationBuilder implements CommonModelBuilder<VacationCount> {
        private final Long memberId;
        private final Float vacationTotal;
        private final Float vacationCount;
        private final LocalDate dateVacationStart;
        private final LocalDate dateVacationEnd;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        public VacationBuilder(Long memberId, LocalDate dateJoin) {
            this.memberId = memberId;
            this.vacationTotal = 0f;
            this.vacationCount = 0f;
            this.dateVacationStart = dateJoin;
            this.dateVacationEnd = dateJoin.plusYears(1).minusDays(1);
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }
        @Override
        public VacationCount build() {
            return new VacationCount(this);
        }
    }
}
