package com.hhk.membermanagement.entity;

import com.hhk.membermanagement.enums.vacation.ApprovalStatus;
import com.hhk.membermanagement.enums.attendance.ShiftWorkType;
import com.hhk.membermanagement.interfaces.CommonModelBuilder;
import com.hhk.membermanagement.model.attendance.AttendanceStateInfoRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * 근태 관리
 */
@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AttendanceState {
    @ApiModelProperty(value = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ApiModelProperty(value = "직원 아이디")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    private Member memberId;
    @ApiModelProperty(value = "근무 형태")
    @Column(nullable = false, length = 40)
    private ShiftWorkType shiftWorkType;
    @ApiModelProperty(value = "사유")
    @Column(nullable = false, length = 30)
    private String shiftWorkReason;
    @ApiModelProperty(value = "승인 상태")
    @Column(nullable = false, length = 30)
    private ApprovalStatus approvalStatus;
    @ApiModelProperty(value = "근무 시간")
    @Column(nullable = false)
    private Short workTime;
    @ApiModelProperty(value = "생성 시간")
    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @ApiModelProperty(value = "수정 시간")
    @Column(nullable = false)
    private LocalDateTime dateUpdate;
    @ApiModelProperty(value = "결재 시간")
    private LocalDateTime dateApprove; // put할 때 들어갈 것
    private AttendanceState(AttendanceStateBuilder builder) {
        this.memberId = builder.memberId;
        this.shiftWorkType = builder.shiftWorkType;
        this.shiftWorkReason = builder.shiftWorkReason;
        this.approvalStatus = builder.approvalStatus;
        this.workTime = builder.workTime;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
        this.dateApprove = builder.dateApprove;
    }
    public static class AttendanceStateBuilder implements CommonModelBuilder<AttendanceState> {
        private final Member memberId;
        private final ShiftWorkType shiftWorkType;
        private final String shiftWorkReason;
        private final ApprovalStatus approvalStatus;
        private final Short workTime;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;
        private final LocalDateTime dateApprove;

        public AttendanceStateBuilder(Member member, AttendanceStateInfoRequest request) {
            this.memberId = member;
            this.shiftWorkType = request.getShiftWorkType();
            this.shiftWorkReason = request.getShiftWorkReason();
            this.approvalStatus = request.getApprovalStatus();
            this.workTime = request.getWorkTime();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
            this.dateApprove = null; // 이게 맞나..
        }

        @Override
        public AttendanceState build() {
            return new AttendanceState(this);
        }
    }


}
