# 직원 관리 API

### LANGUAGE
```
* JAVA 16
* SpringBoot '2.7.2'
```
### Table 설계
![SwaggerList](./images/Table_plan.png)

### 기능 
```
API 20개
```
> - 로그인
>> 1. 일반 직원 로그인
>> 2. 관리자 로그인
> - 직원 정보 관리
>> 1. 직원 정보 등록 (연차 자동 부여)
>> 2. 직원 정보 가져오기
>> 3. 직원 정보 리스트 가져오기
>> 4. 직원정보 수정
>> 5. 직원 관리자 권한 수정
>> 6. 직원 퇴사 처리
> - 프로필 관리
>> 1. 프로필 이미지 등록
>> 2. 프로필 이미지 가져오기
> - 근태 관리
>> 1. 출퇴근 상태 가져오기
>> 2. 나의 근태 리스트 가져오기
>> 3. 근태 상태 수정
> - 연차 수량 관리
>> 1. 기본 연차 수량 등록
>> 2. 연차 수량 정보 가져오기
> - 휴가 관리
>> 1. 휴가 신청 등록
>> 2. 해당직원 휴가 리스트 가져오기
>> 3. 휴가 리스트 가져오기 (승인/대기중/반려)
>> 4. 전체 직원 휴가 리스트 가져오기
>> 5. 휴가 승인 처리 및 연차 수량 변경

### Swagger Main
![SwaggerList](./images/swagger_main.png)
### 로그인
![SwaggerList](./images/login_1.png)
![SwaggerList](./images/login_2.png)
### 직원 정보 관리
![SwaggerList](./images/member_info.png)
### 프로필 관리
![SwaggerList](./images/profile.png)
### 근태 관리
![SwaggerList](./images/dailycheck.png)
### 연차 수량 관리
![SwaggerList](./images/vacation_count_1.png)
![SwaggerList](./images/vacation_count_2.png)
### 휴가 관리
![SwaggerList](./images/vacation.png)

